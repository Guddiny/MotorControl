# DipTrace Auto-Generated DO File
bestsave off
grid via 0.0039
grid wire 0.0039
define (class Dip_Net_Class_0 Net@0 Net@1 AC1 AC2 +110V -110V GND +5V PIN_D2 A5_REL1 A4_REL2 REL1 REL2 Net@13 A2 A0 A7 PIN_6 A3 TAHO_SIG Net@20 Net@21 Net@22 AC2_OUT Net@24 ANC1 ANC2 L Net@28 A1 PIN_8#D6$ Net@31 PIN_3 PIN_4 PIN_5 PIN_9 PIN_10 PIN_11 Net@38 PIN8)
circuit class Dip_Net_Class_0 (use_via DipViaStyle_0)
rule class Dip_Net_Class_0 (width 3.937)
rule class Dip_Net_Class_0 (clearance 3.937)
rule pcb (pin_width_taper down)
bus diagonal
route 20
clean 2
route 25 16
clean 2
filter 5
recorner diagonal
